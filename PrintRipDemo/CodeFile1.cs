﻿using System;
using System.Runtime.InteropServices;


namespace GLOBAL_DEF
{

    public struct BOARDCFGPARAM
    {
        /// <summary>
        /// <summary>
        /// 打印位置到触发点的距离
        /// </summary>
        public float fPrinterToTrigger;
        public float fPhXOffset;
        /// <summary>
        /// A侧：a b c d   B侧: h g f e 从上往下
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public float[] fPhChXoff;
        /// <summary>
        /// 触发保护(mm)
        /// </summary>
        public float fTriggerProtectLen;
        /// <summary>
        /// 喷印模式
        /// </summary>
        public uint nPrintMode;
        /// <summary>
        /// 灰度模式
        /// </summary>
        public uint nGrayBit;
        /// <summary>
        /// 波形选择
        /// </summary>
        public uint nWaveSel;
        /// <summary>
        /// 只读不能修改
        /// </summary>
        public uint nPhType;
        /// <summary>
        /// 
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public uint[] nMapTable;
        /// <summary>
        /// 触发模式：0：外触发(连续)；1：内触发(等间距)；2：内触发(等间隔)
        /// </summary>
        public uint nTrigMode;
        /// <summary>
        /// 
        /// </summary>
        public bool bforbid;
        /// <summary>
        /// 喷头反向安装
        /// </summary>
        public bool bPhInstallCov;
        /// <summary>
        /// 编码器精度
        /// </summary>
        public float fEncoderDpi;
        /// <summary>
        /// 编码反向
        /// </summary>
        public bool bEncoderCov;
        /// <summary>
        /// 触发信号反向处理
        /// </summary>
        public bool bTriggerCov;
        /// <summary>
        /// 闪喷周期
        /// </summary>
        public float fFlashCycle;
        /// <summary>
        /// 闪喷有效时间
        /// </summary>
        public float fFlashVaildTime;
        /// <summary>
        /// 闪喷频率HZ
        /// </summary>
        public float nFlashFreq;
        /// <summary>
        /// 闪喷数据掩码
        /// </summary>
        public uint nFlashDataMask;
        /// <summary>
        /// 闪喷墨点选择
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public ushort[] nFlashJetIndex;
        /// <summary>
        /// 指定喷孔闪喷使能
        /// </summary>
        public bool nFlashSelectJetEnable;
        /// <summary>
        /// 喷头左右镜像
        /// </summary>
        public bool bXImgMirror;
        /// <summary>
        /// 
        /// </summary>
        public uint nMotorDirFlag;
        /// <summary>
        /// 
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public bool[] bCHEnable;
        /// <summary>
        /// 喷头垂直镜像
        /// </summary>
        public bool bYImgMirror;
        /// <summary>
        /// 
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public float[] nWaveValtage;
        /// <summary>
        /// 
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public int[] nColorIndex;
        /// <summary>
        /// 米/分钟
        /// </summary>
        public float fVirspeed;
        /// <summary>
        /// 需要等待第一次触发信号
        /// </summary>
        public bool nNeedWaitFirstTrig;
        /// <summary>
        /// 
        /// </summary>
        public float nModuleEncoderDpi;
        /// <summary>
        /// 基准位置
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public float[] fHeadXOffset;
        /// <summary>
        /// 一头一色的时的 喷头颜色
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public int[] nHeadColorIndex;//喷头颜色
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public float[] nWaveV2;
        public float fExternTrigIgnore;
        public float fExternTrigGapProtect;
        public int nDataMode;//2bit打印时的灰度模式
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
        public int[] nS3200FeatherMode;

        public float fSameColorOffset;
        public int nHeadXOffset;
        public int nHPOverjets;
        public int nBoardColor;
        public int nStopMode;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public float[] v;//预留1
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public int[] nInHeadOverjetDisable;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public int[] nInHeadOverjetAdj;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public float[] fHeadTemp;//喷头温度
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public float[] fScanRevHeadOffset;//一卡一头和一卡多头(一卡最多8个)时，扫描打印喷头间反向偏差

        public int nFeatherAdvance;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public int[] nDecorateRightPer;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public int[] nDecorateLeftPer;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 425)]
        public uint[] nRev;
        public void Init()
        {
            fPhChXoff = new float[8];

            nMapTable = new uint[8];

            nFlashJetIndex = new ushort[8];

            bCHEnable = new bool[8];

            nWaveValtage = new float[4];

            nColorIndex = new int[4];

            fHeadXOffset = new float[8];

            fScanRevHeadOffset = new float[8];

            nHeadColorIndex = new int[8];

            nRev = new uint[425];
        }


    }//请不要对该数据 经行清零操作

   
    public struct SYS_PARAM
    {
        public float fPhToTrigger;//走纸方向的白边 mm  这个值不能为负数，如果需要写入负数就在上面处理好
        public uint nFeatherIndex;//羽化文件编号
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
        public byte[] nOverJets;//重叠嘴C

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
        public byte[] nOverJetsM;//重叠嘴M

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
        public byte[] nOverJetsY;//重叠嘴Y

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
        public byte[] nOverJetsK;//重叠嘴K

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
        public float[] fHeadStdPos;//基准位置

        public int nPrintMode;
        public int nTriggerMode;
        public float fTriggerProtectLen;
        public int nWaveMode;
        public bool nHeadYMirror;
        public bool nHeadXMirror;
        public bool nHeadDir;
        public int nColorBarWidth;//色条高度
        public float fColorBarHeight;//色条宽度
        public int nColorBarPos;//色条位置
        public float fColorBarEdge;//色条边距
        public float fxpos;
        public float fypos;
        public int nColorIndex;
        public int nBarType;
        public bool nClearBk;
        public bool nProductID;
        public int nFontSize;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
        public byte[] nHeadColorIndex;//一头一色的时的 喷头颜色

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public int[] nColorOffset;//0表示c  1表示m 2表示y  3表示k
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
        public float[] fHeadTemperature;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 128)]
        public float[] fHeadStdPosReverseFineTune;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 490)]
        uint[] nRev;//保留区域 889 885 879  878 750
        public void init()
        {
            nOverJets = new byte[128];
            for (int i = 0; i < 128; i++)
                nOverJets[i] = 0;

            nOverJetsM = new byte[128];
            for (int i = 0; i < 128; i++)
                nOverJetsM[i] = 0;

            nOverJetsY = new byte[128];
            for (int i = 0; i < 128; i++)
                nOverJetsY[i] = 0;

            nOverJetsK = new byte[128];
            for (int i = 0; i < 128; i++)
                nOverJetsK[i] = 0;

            fHeadStdPos = new float[128];
            for (int i = 0; i < 128; i++)
                fHeadStdPos[i] = 0;

            nHeadColorIndex = new byte[128];
            for (int i = 0; i < 128; i++)
                nHeadColorIndex[i] = 0;

            nColorOffset = new int[4];
            for (int i = 0; i < 4; i++)
                nColorOffset[i] = 0;

            fHeadTemperature = new float[128];
            for (int i = 0; i < 128; i++)
                fHeadTemperature[i] = 0;

            fHeadStdPosReverseFineTune = new float[128];
            for (int i = 0; i < 128; i++)
                fHeadStdPosReverseFineTune[i] = 0;

            nRev = new uint[490];
            for (int i = 0; i < 490; i++)
                nRev[i] = 0;

            fPhToTrigger = 0;
            nFeatherIndex = 0;
        }
    }
    public class PUBLIC_WAY
    {
        /// <summary>  
        /// 由结构体转换为byte数组  
        /// </summary>  
        public static byte[] GStructureToByte<T>(T structure)
        {
            int size = Marshal.SizeOf(typeof(T));
            byte[] buffer = new byte[size];
            IntPtr bufferIntPtr = Marshal.AllocHGlobal(size);
            try
            {
                Marshal.StructureToPtr(structure, bufferIntPtr, true);
                Marshal.Copy(bufferIntPtr, buffer, 0, size);
            }
            finally
            {
                Marshal.FreeHGlobal(bufferIntPtr);
            }
            return buffer;
        }


        /// <summary>  
        /// 由byte数组转换为结构体  
        /// </summary>  
        public static T GByteToStructure<T>(byte[] dataBuffer)
        {
            object structure = null;
            int size = Marshal.SizeOf(typeof(T));
            IntPtr allocIntPtr = Marshal.AllocHGlobal(size);
            try
            {
                Marshal.Copy(dataBuffer, 0, allocIntPtr, size);
                structure = Marshal.PtrToStructure(allocIntPtr, typeof(T));
            }
            finally
            {
                Marshal.FreeHGlobal(allocIntPtr);
            }
            return (T)structure;
        }
    }
}