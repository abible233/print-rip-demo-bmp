﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using GLOBAL_DEF;




namespace PrintRipDemo
{

   
    public partial class Form2 : Form
    {

        public int _PH_ES_I3200_1H = 0;
        public int _PH_ES_5113_1H = 1;
        public int _PH_ES_S3200_1H = 2;
        public int _PH_GEN_GEN5I_1H = 3;
        public int _PH_GEN_GEN5_1H = 4;
        public int _PH_ES_1600_1H = 5;
        public int _PH_RF_W160T_8H = 6;
        public int _PH_CE4M_1H = 7;
        public int _PH_HP_X214 = 8; //i3200一样 类似于10个喷头
        public int _PH_RF_W160_8H = 9;
        public int _PH_KJ4B_1H = 10;
        public int _PH_KJ4B_2H = 11;
        public int _PH_KJ4B_4H = 12;
        public int _PH_KJ4A_1H = 13;
        public int _PH_KJ4A_2H = 14;
        public int _PH_KJ4A_4H = 15;
        public int _PH_HP_752 = 16;
        public int _PH_SG600 = 17;
        public int _PH_SG1024 = 18;

        public int _PH_SII1536 = 19;
 public int    _PH_SII1024=        20;

 public int   _PH_UNKONW  =        21;
public int  _PH_KJ4B_0600_1H  =       22;
 public int   _PH_KJ4B_0600_2H =        23;
 public int   _PH_KJ4B_0600_4H  =       24;

 public int   _PH_KJ4B_0300_1H =        25;
 public int   _PH_KJ4B_0300_2H =        26;
 public int  _PH_KJ4B_0300_4H  =       27;

 public int   _PH_KJ4B_1200_1H  =       28;
 public int   _PH_KJ4B_1200_2H =        29;
 public int   _PH_KJ4B_1200_4H  =       30;

 public int   _PH_XAAR_2002_1H =       31;


        [DllImport("GoodPrint.dll", EntryPoint = "PTR_GetNetBoardCount", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern int PTR_GetNetBoardCount();//获取单个板卡的数量

        [DllImport("GoodPrint.dll", EntryPoint = "PRT_GetBoardCfgParam", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void PRT_GetBoardCfgParam(int nNetModuleIndex, ref BOARDCFGPARAM pCfg);//获取打印机设置参数

        [DllImport("GoodPrint.dll", EntryPoint = "PRT_SetBoardCfgParam", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void PRT_SetBoardCfgParam(int nNetModuleIndex, ref BOARDCFGPARAM pCfg);//设置打印机设置参数

        [DllImport("GoodPrint.dll", EntryPoint = "PRT_GetModuleHeadType", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern int PRT_GetModuleHeadType(int nNetModuleIndex);

        [DllImport("GoodPrint.dll", EntryPoint = "PRT_GetPrintHeadCount", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern int PRT_GetPrintHeadCount(int NetModuleIndex);
        
        //var
        SYS_PARAM sysparam;
      

        public Form2()
        {
            InitializeComponent();

          
        }

        public void InitDataView()
        {
             


            int nindex;
            int nBoardCount = PTR_GetNetBoardCount();
            string key,value;
            string file = System.Windows.Forms.Application.StartupPath+"\\sysparam.cfg";
            Stream stream;
            int nindex2;
            int nHeadType=PRT_GetModuleHeadType(0);
            int phcount;

            phcount=0;
            for (int i = 0; i < nBoardCount; i++)
                phcount += PRT_GetPrintHeadCount(i);

            if (!File.Exists(file))
            {
                sysparam = new SYS_PARAM();
                sysparam.init();

                // sysparam
                FileStream tempfile = new FileStream(file, FileMode.Create);
                tempfile.Close();

                for (int i = 0; i < phcount; i++)
                {
                    nindex = dataGridView1.Rows.Add();

                    key = string.Format("{0:D}", i + 1);
                    dataGridView1.Rows[nindex].Cells[0].Value = key;
                    dataGridView1.Rows[nindex].Cells[1].Value = "0";
                    dataGridView1.Rows[nindex].Cells[2].Value = "0";

                    nindex2 = dataGridView2.Rows.Add();
                    key = string.Format("{0:D}", i + 1);
                    dataGridView2.Rows[nindex2].Cells[0].Value = key;
                    dataGridView2.Rows[nindex2].Cells[0].Value = "0";
                }
            }
            else
            {
                
                int nSize=Marshal.SizeOf(typeof(SYS_PARAM));
                byte[] buf=new  byte[nSize];

                stream = File.OpenRead(file);
                if (stream.Length > 0)
                {
                    stream.Position = 0;
                    stream.Read(buf, 0, nSize);
                    stream.Close();
                    sysparam = PUBLIC_WAY.GByteToStructure<SYS_PARAM>(buf);
                    for (int i = 0; i < phcount; i++)
                    {
                        nindex = dataGridView1.Rows.Add();
                        key = string.Format("{0:D}", i + 1);
                        dataGridView1.Rows[nindex].Cells[0].Value = key;

                        value = string.Format("{0:0.###}", sysparam.fHeadStdPos[i]);
                        dataGridView1.Rows[nindex].Cells[1].Value = value;

                        value = string.Format("{0:0.###}", sysparam.fHeadTemperature[i]);
                        dataGridView1.Rows[nindex].Cells[3].Value = value;

                        value = string.Format("{0:0.###}", sysparam.fHeadStdPosReverseFineTune[i]);
                        dataGridView1.Rows[nindex].Cells[4].Value = value;

                        if (nHeadType == _PH_RF_W160T_8H || nHeadType == _PH_RF_W160_8H || nHeadType == _PH_KJ4B_1H 
                            || nHeadType == _PH_KJ4B_2H || nHeadType == _PH_KJ4B_4H || nHeadType == _PH_SG1024
                            || nHeadType == _PH_ES_5113_1H || nHeadType == _PH_XAAR_2002_1H)
                        {
                            value = string.Format("{0:D}", sysparam.nHeadColorIndex[i]);
                            dataGridView1.Rows[nindex].Cells[2].Value = value;
                        }else {

                            if (i > 0)
                            {
                                nindex2 = dataGridView2.Rows.Add();
                                key = string.Format("{0:D}", i);
                                dataGridView2.Rows[nindex2].Cells[0].Value = key;

                                value = string.Format("{0:D}", sysparam.nOverJets[nindex2]);
                                dataGridView2.Rows[nindex2].Cells[1].Value = value;
                            }
                        }

                    }

                    if (nHeadType == _PH_RF_W160T_8H || nHeadType == _PH_RF_W160_8H || nHeadType == _PH_KJ4B_1H 
                        || nHeadType == _PH_KJ4B_2H || nHeadType == _PH_KJ4B_4H || nHeadType == _PH_SG1024
                        || nHeadType == _PH_ES_5113_1H || nHeadType==_PH_XAAR_2002_1H)
                    {


                        groupBox3.Visible = true;
                        label20.Visible = true;
                        textBox8.Visible = true;
                        textBox8.Text = string.Format("{0:D}", sysparam.nColorOffset[0]);

                        label21.Visible = true;
                        textBox9.Visible = true;
                        textBox9.Text = string.Format("{0:D}", sysparam.nColorOffset[1]);

                        label22.Visible = true;
                        textBox10.Visible = true;
                        textBox10.Text = string.Format("{0:D}", sysparam.nColorOffset[2]);

                        label23.Visible = true;
                        textBox11.Visible = true;
                        textBox11.Text = string.Format("{0:D}", sysparam.nColorOffset[3]);


                        int nMaxHeightHeadCount=0;
				        int[] nModuleColorIndex=new int[4];


                        for (int i = 0; i < 4; i++)
                            nModuleColorIndex[i] = 0;

				        for(int m=0;m<phcount;m++)
				        {

                            if (sysparam.nHeadColorIndex[m] == 0)
							    nModuleColorIndex[0]++;

                            if (sysparam.nHeadColorIndex[m] == 1)
							    nModuleColorIndex[1]++;

                            if (sysparam.nHeadColorIndex[m] == 2)
							    nModuleColorIndex[2]++;

                            if (sysparam.nHeadColorIndex[m] == 3)
							    nModuleColorIndex[3]++;
					        
				        }

				        for(int c=0;c<4;c++)
				        {
					        if(nModuleColorIndex[c]>nMaxHeightHeadCount)
						        nMaxHeightHeadCount=nModuleColorIndex[c];
				        }



                        dataGridView2.Columns[1].HeaderText = "重叠嘴C(像素)";
                        dataGridView2.Columns[2].Visible = true;
                        dataGridView2.Columns[3].Visible = true;
                        dataGridView2.Columns[4].Visible = true;
                        for (int i = 0; i < nMaxHeightHeadCount-1; i++)
                        {
                            nindex2 = dataGridView2.Rows.Add();
                            key = string.Format("{0:D}", i);
                            dataGridView2.Rows[nindex2].Cells[0].Value = key;

                            key = string.Format("{0:D}", sysparam.nOverJets[i]);
                            dataGridView2.Rows[nindex2].Cells[1].Value = key;

                            key = string.Format("{0:D}", sysparam.nOverJetsM[i]);
                            dataGridView2.Rows[nindex2].Cells[2].Value = key;

                            key = string.Format("{0:D}", sysparam.nOverJetsY[i]);
                            dataGridView2.Rows[nindex2].Cells[3].Value = key;

                            key = string.Format("{0:D}", sysparam.nOverJetsK[i]);
                            dataGridView2.Rows[nindex2].Cells[4].Value = key;
                        }
                    }

                    textBox1.Text = string.Format("{0:0.###}", sysparam.fPhToTrigger);
                    textBox2.Text = string.Format("{0:D}", sysparam.nFeatherIndex);
                    textBox3.Text = string.Format("{0:0.###}", sysparam.fTriggerProtectLen);
                    comboBox1.SelectedIndex = sysparam.nPrintMode;
                    comboBox2.SelectedIndex = sysparam.nTriggerMode;
                    comboBox3.SelectedIndex = sysparam.nWaveMode;
                    checkBox1.Checked = sysparam.nHeadYMirror;
                    checkBox2.Checked = sysparam.nHeadXMirror;
                    checkBox3.Checked = sysparam.nHeadDir;

                    comboBox4.SelectedIndex = sysparam.nColorBarWidth;
                    comboBox5.SelectedIndex = sysparam.nColorBarPos;

                    textBox4.Text = string.Format("{0:0.###}", sysparam.fColorBarHeight);
                    textBox5.Text = string.Format("{0:0.###}", sysparam.fColorBarEdge);

                    textBox6.Text = string.Format("{0:0.###}", sysparam.fxpos);
                    textBox7.Text = string.Format("{0:0.###}", sysparam.fypos);

                    comboBox6.SelectedIndex = sysparam.nColorIndex;
                    comboBox7.SelectedIndex = sysparam.nBarType;

                    checkBox4.Checked = sysparam.nClearBk;
                    checkBox5.Checked = sysparam.nProductID;

                    comboBox8.SelectedIndex = sysparam.nFontSize;
                }
                else
                {
                    sysparam = new SYS_PARAM();
                    sysparam.init();

                    for (int i = 0; i < phcount; i++)
                    {
                        nindex = dataGridView1.Rows.Add();
                        key = string.Format("{0:D}", i + 1);
                        dataGridView1.Rows[nindex].Cells[0].Value = key;

                        value = string.Format("{0:D}", 0);
                        dataGridView1.Rows[nindex].Cells[1].Value =value;

                        value = string.Format("{0:D}", 0);
                        dataGridView1.Rows[nindex].Cells[2].Value = value;

                        value = string.Format("{0:D}", 0);
                        dataGridView1.Rows[nindex].Cells[3].Value = value;


                        nindex2 = dataGridView2.Rows.Add();
                        key = string.Format("{0:D}", i + 1);
                        dataGridView2.Rows[nindex2].Cells[0].Value = key;
                        dataGridView2.Rows[nindex2].Cells[0].Value = "0";
                    }
                }

                stream.Close();

            }
        }

        private void dataGridView1_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            //判断是否可以编辑  
            if (dgv.Columns[e.ColumnIndex].Name == "Column1")
            {
                //编辑不能  
                e.Cancel = true;
            }
        }


        //确定保存参数
        private void button1_Click(object sender, EventArgs e)
        {
            int phcount = dataGridView1.RowCount;
            string file = System.Windows.Forms.Application.StartupPath + "\\sysparam.cfg";
            Stream stream;
            int nSize=Marshal.SizeOf(typeof(SYS_PARAM));
            byte[] buf;
            int nBoardCount;
            int novercount = dataGridView2.RowCount;
            int nHeadType = PRT_GetModuleHeadType(0);
            int nHeadCount;
            float []fHeadOffset=new float[8];
            int[] nHeadColorIndex = new int[8];
            int  npos=0;
            float []  fheadtemp = new float[128];
            float []  fHeadStdPosReverseFineTune=new float[128];
            //将参数保存到本地

            for (int i = 0; i < 8; i++)
            {
                fHeadOffset[i] = 0.0f;
            }

            sysparam.fPhToTrigger = (float)System.Convert.ToDouble(textBox1.Text);
            sysparam.nFeatherIndex =(uint) System.Convert.ToInt32(textBox2.Text);
            sysparam.nPrintMode = comboBox1.SelectedIndex;
            sysparam.nTriggerMode = comboBox2.SelectedIndex;
            sysparam.fTriggerProtectLen = (float)System.Convert.ToDouble(textBox3.Text);
            sysparam.nWaveMode = comboBox3.SelectedIndex;
            sysparam.nHeadYMirror = checkBox1.Checked;
            sysparam.nHeadXMirror = checkBox2.Checked;
            sysparam.nHeadDir = checkBox3.Checked;
            sysparam.nColorBarWidth = comboBox4.SelectedIndex;
            sysparam.fColorBarHeight = (float)System.Convert.ToDouble(textBox4.Text);
            sysparam.nColorBarPos = comboBox5.SelectedIndex;
            sysparam.fColorBarEdge = (float)System.Convert.ToDouble(textBox5.Text);
            sysparam.fxpos=(float)System.Convert.ToDouble(textBox6.Text);
            sysparam.fypos=(float)System.Convert.ToDouble(textBox7.Text);;
            sysparam.nColorIndex = comboBox6.SelectedIndex;
            sysparam.nBarType = comboBox7.SelectedIndex;
            sysparam.nClearBk = checkBox4.Checked;
            sysparam.nProductID = checkBox5.Checked;
            sysparam.nFontSize = comboBox8.SelectedIndex;

            
            for(int i=0;i<phcount;i++)
            {
                sysparam.fHeadStdPos[i] = (float)System.Convert.ToDouble(dataGridView1.Rows[i].Cells[1].Value);
                sysparam.nHeadColorIndex[i] =(byte)System.Convert.ToByte(dataGridView1.Rows[i].Cells[2].Value);
                sysparam.fHeadTemperature[i] = (byte)System.Convert.ToByte(dataGridView1.Rows[i].Cells[3].Value);
                sysparam.fHeadStdPosReverseFineTune[i] = (byte)System.Convert.ToByte(dataGridView1.Rows[i].Cells[4].Value);
            }

            for (int i = 0; i < novercount; i++)
            {
                sysparam.nOverJets[i] = System.Convert.ToByte(dataGridView2.Rows[i].Cells[1].Value);

                sysparam.nOverJetsM[i] = System.Convert.ToByte(dataGridView2.Rows[i].Cells[2].Value);

                sysparam.nOverJetsY[i] = System.Convert.ToByte(dataGridView2.Rows[i].Cells[3].Value);

                sysparam.nOverJetsK[i] = System.Convert.ToByte(dataGridView2.Rows[i].Cells[4].Value);
            }


            if (nHeadType == _PH_RF_W160T_8H || nHeadType == _PH_RF_W160_8H || nHeadType == _PH_KJ4B_1H || nHeadType == _PH_KJ4B_2H 
                || nHeadType == _PH_KJ4B_4H || nHeadType == _PH_SG1024
                || nHeadType == _PH_ES_5113_1H || nHeadType==_PH_XAAR_2002_1H)
            {
                sysparam.nColorOffset[0] = System.Convert.ToInt32(textBox8.Text);
                sysparam.nColorOffset[1] = System.Convert.ToInt32(textBox9.Text);
                sysparam.nColorOffset[2] = System.Convert.ToInt32(textBox10.Text);
                sysparam.nColorOffset[3] = System.Convert.ToInt32(textBox11.Text);
            }

            stream = File.OpenWrite(file);
            buf = PUBLIC_WAY.GStructureToByte<SYS_PARAM>(sysparam);

            stream.Write(buf, 0, nSize);

            stream.Close();

            this.Close();

            //将参数保存到打印机
            BOARDCFGPARAM pCfg = new BOARDCFGPARAM();
            nBoardCount = PTR_GetNetBoardCount();
            npos=0;
            for (int i = 0; i < nBoardCount; i++)
            {
                PRT_GetBoardCfgParam(i, ref pCfg);
                pCfg.bYImgMirror = sysparam.nHeadYMirror;
                pCfg.bXImgMirror = sysparam.nHeadXMirror;
                pCfg.bPhInstallCov = sysparam.nHeadDir;


                if (nHeadType == _PH_RF_W160T_8H || nHeadType == _PH_RF_W160_8H || nHeadType == _PH_KJ4B_1H || nHeadType == _PH_KJ4B_2H 
                    || nHeadType == _PH_KJ4B_4H || nHeadType == _PH_SG1024
                    || nHeadType == _PH_ES_5113_1H || nHeadType==_PH_XAAR_2002_1H)
                {
                    nHeadCount=PRT_GetPrintHeadCount(i);
                    for(int n=0;n<nHeadCount;n++)
                    {
                        fHeadOffset[n] = sysparam.fHeadStdPos[npos + n];
                        nHeadColorIndex[n] = sysparam.nHeadColorIndex[npos + n];
                        fheadtemp[n] = sysparam.fHeadTemperature[npos + n];
                        fHeadStdPosReverseFineTune[n] = sysparam.fHeadStdPosReverseFineTune[npos + n];
                
                    }
                    npos += nHeadCount;
                    for (int n = 0; n < nHeadCount; n++)
                    {
                        pCfg.fHeadXOffset[n] = fHeadOffset[n];
                        pCfg.nHeadColorIndex[n] = nHeadColorIndex[n];
                        pCfg.fHeadTemp[n] = fheadtemp[n];//一卡多头 温度
                        pCfg.fScanRevHeadOffset[n] = fHeadStdPosReverseFineTune[n];
                    }
                }else
                    pCfg.fPrinterToTrigger = sysparam.fHeadStdPos[i];
                pCfg.nPrintMode = (uint)sysparam.nPrintMode;
                pCfg.nTrigMode = (uint)sysparam.nTriggerMode;
                pCfg.nWaveSel = (uint)sysparam.nWaveMode;
                pCfg.fTriggerProtectLen = sysparam.fTriggerProtectLen;

                PRT_SetBoardCfgParam(i, ref pCfg);
            }
            
        }
        //取消 不保存
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
