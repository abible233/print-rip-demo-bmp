﻿namespace PrintRipDemo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button2 = new System.Windows.Forms.Button();
            this.startbutton = new System.Windows.Forms.Button();
            this.stopbutton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button31 = new System.Windows.Forms.Button();
            this.textBox2_cmyk = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.sysmsgbox = new System.Windows.Forms.TextBox();
            this.prttagbox = new System.Windows.Forms.TextBox();
            this.sendTagBox = new System.Windows.Forms.TextBox();
            this.spdbox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox_prn = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pictureBox_k = new System.Windows.Forms.PictureBox();
            this.pictureBox_y = new System.Windows.Forms.PictureBox();
            this.pictureBox_m = new System.Windows.Forms.PictureBox();
            this.pictureBox_c = new System.Windows.Forms.PictureBox();
            this.button6 = new System.Windows.Forms.Button();
            this.textBox_y = new System.Windows.Forms.TextBox();
            this.textBox_k = new System.Windows.Forms.TextBox();
            this.textBox_m = new System.Windows.Forms.TextBox();
            this.textBox_c = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_k)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_y)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_m)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_c)).BeginInit();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(14, 24);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "喷印设置";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // startbutton
            // 
            this.startbutton.Location = new System.Drawing.Point(10, 41);
            this.startbutton.Margin = new System.Windows.Forms.Padding(2);
            this.startbutton.Name = "startbutton";
            this.startbutton.Size = new System.Drawing.Size(79, 27);
            this.startbutton.TabIndex = 4;
            this.startbutton.Text = "启动喷印";
            this.startbutton.UseVisualStyleBackColor = true;
            this.startbutton.Click += new System.EventHandler(this.button3_Click);
            // 
            // stopbutton
            // 
            this.stopbutton.Enabled = false;
            this.stopbutton.Location = new System.Drawing.Point(10, 85);
            this.stopbutton.Margin = new System.Windows.Forms.Padding(2);
            this.stopbutton.Name = "stopbutton";
            this.stopbutton.Size = new System.Drawing.Size(79, 27);
            this.stopbutton.TabIndex = 4;
            this.stopbutton.Text = "停止喷印";
            this.stopbutton.UseVisualStyleBackColor = true;
            this.stopbutton.Click += new System.EventHandler(this.button4_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 75);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "喷印数量:";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(90, 71);
            this.textBox3.Margin = new System.Windows.Forms.Padding(2);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(68, 21);
            this.textBox3.TabIndex = 6;
            this.textBox3.Text = "1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 101);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "喷印精度(dpi):";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Controls.Add(this.button31);
            this.groupBox2.Controls.Add(this.textBox2_cmyk);
            this.groupBox2.Controls.Add(this.textBox4);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.textBox3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.ForeColor = System.Drawing.SystemColors.Highlight;
            this.groupBox2.Location = new System.Drawing.Point(547, 11);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(184, 162);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "参数设置";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "1bit",
            "2bit"});
            this.comboBox1.Location = new System.Drawing.Point(90, 125);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(68, 20);
            this.comboBox1.TabIndex = 9;
            this.comboBox1.Visible = false;
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(104, 24);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(75, 23);
            this.button31.TabIndex = 8;
            this.button31.Text = "系统选项";
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Click += new System.EventHandler(this.button31_Click);
            // 
            // textBox2_cmyk
            // 
            this.textBox2_cmyk.Location = new System.Drawing.Point(90, 98);
            this.textBox2_cmyk.Margin = new System.Windows.Forms.Padding(2);
            this.textBox2_cmyk.Name = "textBox2_cmyk";
            this.textBox2_cmyk.Size = new System.Drawing.Size(68, 21);
            this.textBox2_cmyk.TabIndex = 7;
            this.textBox2_cmyk.Text = "600";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(90, 98);
            this.textBox4.Margin = new System.Windows.Forms.Padding(2);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(68, 21);
            this.textBox4.TabIndex = 7;
            this.textBox4.Text = "600";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 128);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 12);
            this.label7.TabIndex = 5;
            this.label7.Text = "位深度:";
            this.label7.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkBox1);
            this.groupBox3.Controls.Add(this.stopbutton);
            this.groupBox3.Controls.Add(this.startbutton);
            this.groupBox3.Location = new System.Drawing.Point(547, 211);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(184, 168);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "喷印控制";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.sysmsgbox);
            this.groupBox4.Controls.Add(this.prttagbox);
            this.groupBox4.Controls.Add(this.sendTagBox);
            this.groupBox4.Controls.Add(this.spdbox);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.groupBox4.Location = new System.Drawing.Point(547, 417);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox4.Size = new System.Drawing.Size(184, 197);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "系统信息";
            this.groupBox4.Enter += new System.EventHandler(this.groupBox4_Enter);
            // 
            // sysmsgbox
            // 
            this.sysmsgbox.Location = new System.Drawing.Point(61, 145);
            this.sysmsgbox.Margin = new System.Windows.Forms.Padding(2);
            this.sysmsgbox.Name = "sysmsgbox";
            this.sysmsgbox.ReadOnly = true;
            this.sysmsgbox.Size = new System.Drawing.Size(95, 21);
            this.sysmsgbox.TabIndex = 4;
            // 
            // prttagbox
            // 
            this.prttagbox.Location = new System.Drawing.Point(61, 103);
            this.prttagbox.Margin = new System.Windows.Forms.Padding(2);
            this.prttagbox.Name = "prttagbox";
            this.prttagbox.ReadOnly = true;
            this.prttagbox.Size = new System.Drawing.Size(59, 21);
            this.prttagbox.TabIndex = 4;
            // 
            // sendTagBox
            // 
            this.sendTagBox.Location = new System.Drawing.Point(61, 61);
            this.sendTagBox.Margin = new System.Windows.Forms.Padding(2);
            this.sendTagBox.Name = "sendTagBox";
            this.sendTagBox.ReadOnly = true;
            this.sendTagBox.Size = new System.Drawing.Size(59, 21);
            this.sendTagBox.TabIndex = 4;
            // 
            // spdbox
            // 
            this.spdbox.Location = new System.Drawing.Point(116, 22);
            this.spdbox.Margin = new System.Windows.Forms.Padding(2);
            this.spdbox.Name = "spdbox";
            this.spdbox.ReadOnly = true;
            this.spdbox.Size = new System.Drawing.Size(39, 21);
            this.spdbox.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 149);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 3;
            this.label6.Text = "打印状态：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 104);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "已打印：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 63);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 1;
            this.label4.Text = "已发送：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 26);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "走纸速度(m/分钟):";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(6, 57);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(437, 21);
            this.textBox1.TabIndex = 0;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(6, 82);
            this.textBox5.Margin = new System.Windows.Forms.Padding(2);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(437, 21);
            this.textBox5.TabIndex = 0;
            this.textBox5.Visible = false;
            this.textBox5.Click += new System.EventHandler(this.m_click);
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(6, 107);
            this.textBox6.Margin = new System.Windows.Forms.Padding(2);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(437, 21);
            this.textBox6.TabIndex = 0;
            this.textBox6.Visible = false;
            this.textBox6.Click += new System.EventHandler(this.y_click);
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(6, 132);
            this.textBox7.Margin = new System.Windows.Forms.Padding(2);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(437, 21);
            this.textBox7.TabIndex = 0;
            this.textBox7.Visible = false;
            this.textBox7.Click += new System.EventHandler(this.k_click);
            // 
            // textBox_prn
            // 
            this.textBox_prn.Location = new System.Drawing.Point(6, 545);
            this.textBox_prn.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_prn.Multiline = true;
            this.textBox_prn.Name = "textBox_prn";
            this.textBox_prn.ReadOnly = true;
            this.textBox_prn.Size = new System.Drawing.Size(515, 65);
            this.textBox_prn.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(447, 54);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(74, 25);
            this.button1.TabIndex = 1;
            this.button1.Text = "Browse";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(447, 79);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(74, 25);
            this.button3.TabIndex = 1;
            this.button3.Text = "Browse-M";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button7_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(447, 104);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(74, 25);
            this.button4.TabIndex = 1;
            this.button4.Text = "Browse-Y";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Visible = false;
            this.button4.Click += new System.EventHandler(this.button8_Click);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(447, 129);
            this.button5.Margin = new System.Windows.Forms.Padding(2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(74, 25);
            this.button5.TabIndex = 1;
            this.button5.Text = "Browse-K";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Visible = false;
            this.button5.Click += new System.EventHandler(this.button9_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Location = new System.Drawing.Point(5, 159);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(515, 377);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tabControl1);
            this.groupBox1.Controls.Add(this.pictureBox_k);
            this.groupBox1.Controls.Add(this.pictureBox_y);
            this.groupBox1.Controls.Add(this.pictureBox_m);
            this.groupBox1.Controls.Add(this.pictureBox_c);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.textBox_y);
            this.groupBox1.Controls.Add(this.textBox_k);
            this.groupBox1.Controls.Add(this.textBox_m);
            this.groupBox1.Controls.Add(this.textBox_c);
            this.groupBox1.Controls.Add(this.textBox_prn);
            this.groupBox1.Controls.Add(this.textBox7);
            this.groupBox1.Controls.Add(this.textBox6);
            this.groupBox1.Controls.Add(this.textBox5);
            this.groupBox1.Controls.Add(this.textBox8);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Location = new System.Drawing.Point(6, 3);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(537, 611);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "图像";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(6, 29);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(515, 23);
            this.tabControl1.TabIndex = 3;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.selectindex);
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(507, 0);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(507, 0);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pictureBox_k
            // 
            this.pictureBox_k.BackColor = System.Drawing.Color.White;
            this.pictureBox_k.Location = new System.Drawing.Point(5, 159);
            this.pictureBox_k.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox_k.Name = "pictureBox_k";
            this.pictureBox_k.Size = new System.Drawing.Size(515, 377);
            this.pictureBox_k.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_k.TabIndex = 2;
            this.pictureBox_k.TabStop = false;
            this.pictureBox_k.Visible = false;
            // 
            // pictureBox_y
            // 
            this.pictureBox_y.BackColor = System.Drawing.Color.White;
            this.pictureBox_y.Location = new System.Drawing.Point(5, 159);
            this.pictureBox_y.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox_y.Name = "pictureBox_y";
            this.pictureBox_y.Size = new System.Drawing.Size(515, 377);
            this.pictureBox_y.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_y.TabIndex = 2;
            this.pictureBox_y.TabStop = false;
            this.pictureBox_y.Visible = false;
            // 
            // pictureBox_m
            // 
            this.pictureBox_m.BackColor = System.Drawing.Color.White;
            this.pictureBox_m.Location = new System.Drawing.Point(5, 159);
            this.pictureBox_m.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox_m.Name = "pictureBox_m";
            this.pictureBox_m.Size = new System.Drawing.Size(515, 377);
            this.pictureBox_m.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_m.TabIndex = 2;
            this.pictureBox_m.TabStop = false;
            this.pictureBox_m.Visible = false;
            // 
            // pictureBox_c
            // 
            this.pictureBox_c.BackColor = System.Drawing.Color.White;
            this.pictureBox_c.Location = new System.Drawing.Point(5, 159);
            this.pictureBox_c.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox_c.Name = "pictureBox_c";
            this.pictureBox_c.Size = new System.Drawing.Size(515, 377);
            this.pictureBox_c.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_c.TabIndex = 2;
            this.pictureBox_c.TabStop = false;
            this.pictureBox_c.Visible = false;
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(446, 54);
            this.button6.Margin = new System.Windows.Forms.Padding(2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(74, 25);
            this.button6.TabIndex = 1;
            this.button6.Text = "Browse-C";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Visible = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // textBox_y
            // 
            this.textBox_y.Location = new System.Drawing.Point(6, 545);
            this.textBox_y.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_y.Multiline = true;
            this.textBox_y.Name = "textBox_y";
            this.textBox_y.ReadOnly = true;
            this.textBox_y.Size = new System.Drawing.Size(515, 65);
            this.textBox_y.TabIndex = 0;
            this.textBox_y.Visible = false;
            // 
            // textBox_k
            // 
            this.textBox_k.Location = new System.Drawing.Point(6, 545);
            this.textBox_k.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_k.Multiline = true;
            this.textBox_k.Name = "textBox_k";
            this.textBox_k.ReadOnly = true;
            this.textBox_k.Size = new System.Drawing.Size(515, 65);
            this.textBox_k.TabIndex = 0;
            this.textBox_k.Visible = false;
            // 
            // textBox_m
            // 
            this.textBox_m.Location = new System.Drawing.Point(6, 545);
            this.textBox_m.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_m.Multiline = true;
            this.textBox_m.Name = "textBox_m";
            this.textBox_m.ReadOnly = true;
            this.textBox_m.Size = new System.Drawing.Size(515, 65);
            this.textBox_m.TabIndex = 0;
            this.textBox_m.Visible = false;
            // 
            // textBox_c
            // 
            this.textBox_c.Location = new System.Drawing.Point(6, 545);
            this.textBox_c.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_c.Multiline = true;
            this.textBox_c.Name = "textBox_c";
            this.textBox_c.ReadOnly = true;
            this.textBox_c.Size = new System.Drawing.Size(515, 65);
            this.textBox_c.TabIndex = 0;
            this.textBox_c.Visible = false;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(6, 56);
            this.textBox8.Margin = new System.Windows.Forms.Padding(2);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(437, 21);
            this.textBox8.TabIndex = 0;
            this.textBox8.Visible = false;
            this.textBox8.Click += new System.EventHandler(this.c_click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(14, 138);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(96, 16);
            this.checkBox1.TabIndex = 5;
            this.checkBox1.Text = "喷头数据一致";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(750, 619);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Demo";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.form_close);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_k)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_y)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_m)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_c)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button startbutton;
        private System.Windows.Forms.Button stopbutton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox spdbox;
        private System.Windows.Forms.TextBox sysmsgbox;
        private System.Windows.Forms.TextBox prttagbox;
        private System.Windows.Forms.TextBox sendTagBox;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox_prn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.PictureBox pictureBox_c;
        private System.Windows.Forms.TextBox textBox_c;
        private System.Windows.Forms.PictureBox pictureBox_k;
        private System.Windows.Forms.PictureBox pictureBox_y;
        private System.Windows.Forms.PictureBox pictureBox_m;
        private System.Windows.Forms.TextBox textBox_y;
        private System.Windows.Forms.TextBox textBox_k;
        private System.Windows.Forms.TextBox textBox_m;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox2_cmyk;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}

